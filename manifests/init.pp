class gcc {
  case $::operatingsystem {
    debian:  { include gcc::debian }
    default: { include gcc::base }
  }
}
